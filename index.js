// lab 1
var btnBai1 = document.querySelector("#btnBai1");

function khuVuc() {
  var diemKhuVuc = document.querySelector("#khuVuc").value;
  switch (diemKhuVuc) {
    case "A":
      return 2;
    case "B":
      return 1;
    case "C":
      return 0.5;

    default:
      return 0;
  }
}

function doiTuong() {
  var diemDoiTuong = document.querySelector("#doiTuong").value;
  switch (diemDoiTuong) {
    case "1":
      return 2.5;
    case "2":
      return 1.5;
    case "3":
      return 1;

    default:
      return 0;
  }
}

function diemUuTien() {
  return khuVuc() + doiTuong();
}

btnBai1.addEventListener("click", function () {
  var diemChuan = document.querySelector("#diemChuan").value * 1;
  var diemMon1 = document.querySelector("#diemMon1").value * 1;
  var diemMon2 = document.querySelector("#diemMon2").value * 1;
  var diemMon3 = document.querySelector("#diemMon3").value * 1;

  var diemTongKet = diemMon1 + diemMon2 + diemMon3 + diemUuTien();
  var ketQua = "";

  if (diemMon1 <= 0 || diemMon2 <= 0 || diemMon3 <= 0) {
    document.querySelector(
      "#kqBai1"
    ).innerHTML = `Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0`;
  } else {
    if (diemTongKet < diemChuan) {
      ketQua = "Bạn đã rớt. Điểm tổng: ";
    } else {
      ketQua = "Bạn đã đậu. Điểm tổng: ";
    }
    document.querySelector("#kqBai1").innerHTML = ketQua + diemTongKet;
  }
});

//lab 2
var btnBai2 = document.querySelector("#btnBai2");

function donGia(soKw) {
  if (soKw <= 50) {
    return 500;
  } else if (soKw <= 100) {
    return 650;
  } else if (soKw <= 200) {
    return 850;
  } else if (soKw <= 350) {
    return 1100;
  } else {
    return 1300;
  }
}

btnBai2.addEventListener("click", function () {
  var hoTen = document.querySelector("#hoTen").value;
  var soKwEL = document.querySelector("#soKw").value * 1;
  var soKwELTemp = 0;
  var ketQua = 0;
  if (soKwEL <= 0) {
    alert("Số kw nhập không hợp lệ!");
  } else if (soKwEL <= 50) {
    ketQua = donGia(soKwEL) * soKwEL;
  } else if (soKwEL <= 100) {
    soKwELTemp = soKwEL - 50;
    ketQua =
      donGia(soKwEL) * soKwELTemp +
      donGia(soKwEL - soKwELTemp) * (soKwEL - soKwELTemp);
  } else if (soKwEL <= 200) {
    console.log(donGia(soKwEL));
    ketQua = 500 * 50 + 650 * 50 + donGia(soKwEL) * (soKwEL - 100);
  } else if (soKwEL <= 350) {
    ketQua = 500 * 50 + 650 * 50 + 100 * 850 + donGia(soKwEL) * (soKwEL - 200);
  } else {
    ketQua =
      500 * 50 +
      650 * 50 +
      850 * 100 +
      1100 * 150 +
      donGia(soKwEL) * (soKwEL - 350);
  }
  document.querySelector(
    "#kqBai2"
  ).innerHTML = `Họ tên: ${hoTen}; Tiền điện: ${ketQua.toLocaleString()} (VNĐ)`;
});
//lab 3
function thue(thue) {
  if (thue <= 60e6) {
    return 0.05;
  }
  if (thue <= 120e6) {
    return 0.1;
  }
  if (thue <= 210e6) {
    return 0.15;
  }
  if (thue <= 384e6) {
    return 0.2;
  }
  if (thue <= 624e6) {
    return 0.25;
  }
  if (thue <= 960e6) {
    return 0.3;
  }
  if (thue > 960e6) {
    return 0.35;
  }
}

function tongThuNhapThue(tongThuNhap, soNguoiPhuThuoc) {
  return tongThuNhap - 4e6 - soNguoiPhuThuoc * 16e5;
}

var btnBai3 = document.querySelector("#btnBai3");

btnBai3.addEventListener("click", function () {
  var hoVaTen = document.querySelector("#hoVaTen").value;
  var tongThuNhap = document.querySelector("#tongThuNhap").value;
  var soNguoiPhuThuoc = document.querySelector("#soNguoiPhuThuoc").value * 1;

  var result = 0;
  var thuNhap = tongThuNhapThue(tongThuNhap, soNguoiPhuThuoc);

  if (thuNhap <= 60e6) {
    result = thuNhap * thue(thuNhap);
  } else if (thuNhap <= 120e6) {
    result = thuNhap * thue(thuNhap);
  } else if (thuNhap <= 210e6) {
    result = thuNhap * thue(thuNhap);
  } else if (thuNhap <= 384e6) {
    result = thuNhap * thue(thuNhap);
  } else if (thuNhap <= 624e6) {
    result = thuNhap * thue(thuNhap);
  } else if (thuNhap <= 960e6) {
    console.log(thuNhap);
    console.log(thue(thuNhap));
    result = thuNhap * thue(thuNhap);
  } else {
    result = thuNhap * thue(thuNhap);
  }

  var ketQua = Intl.NumberFormat("de-DE", {
    style: "currency",
    currency: "VND",
  }).format(result);
  document.querySelector(
    "#kqBai3"
  ).innerHTML = `Họ tên: ${hoVaTen}; Tiền thuế thu nhập cá nhân: ${ketQua}`;
});
// lab 4

var btnBai4 = document.querySelector("#btnBai4");

function phiHoaDon(loaiKhachHang) {
  switch (loaiKhachHang) {
    case "ND":
      return 4.5;
    case "DN":
      return 15;
    default:
      return 0;
  }
}

function phiDichVu(loaiKhachHang, soKetNoi) {
  switch (loaiKhachHang) {
    case "ND":
      return 20.5;
    case "DN":
      if (soKetNoi <= 10) {
        return 75;
      } else {
        return 75 + (soKetNoi - 10) * 5;
      }
    default:
      return 0;
  }
}

function kenhCaoCap(loaiKhachHang, soKenhCaoCap) {
  switch (loaiKhachHang) {
    case "ND":
      return 7.5 * soKenhCaoCap;
    case "DN":
      return 50 * soKenhCaoCap;
    default:
      return 0;
  }
}

function myFunction() {
  var loaiKhachHang = document.querySelector("#loaiKhachHang").value;
  if (loaiKhachHang == "DN") {
    document.querySelector("#soKetNoi").style.display = "block";
  } else {
    document.querySelector("#soKetNoi").style.display = "none";
  }
}

function tienNhaDan(loaiKhachHang, soKenhCaoCap) {
  return (
    phiHoaDon(loaiKhachHang) +
    phiDichVu(loaiKhachHang, 0) +
    kenhCaoCap(loaiKhachHang, soKenhCaoCap)
  );
}

function tienDoanhNghiep(loaiKhachHang, soKenhCaoCap, soKetNoi) {
  return (
    phiHoaDon(loaiKhachHang) +
    phiDichVu(loaiKhachHang, soKetNoi) +
    kenhCaoCap(loaiKhachHang, soKenhCaoCap)
  );
}

btnBai4.addEventListener("click", function () {
  var loaiKhachHangEL = document.querySelector("#loaiKhachHang").value;
  var maKhachHangEL = document.querySelector("#maKhachHang").value;
  var soKenhCaoCapEL = document.querySelector("#soKenhCaoCap").value * 1;
  var soKetNoiEL = document.querySelector("#soKetNoi").value * 1;

  if (loaiKhachHangEL == "ND") {
    document.querySelector(
      "#kqBai4"
    ).innerHTML = `Mã khách hàng: ${maKhachHangEL}; Tiền cáp: $${tienNhaDan(
      loaiKhachHangEL,
      soKenhCaoCapEL
    ).toLocaleString()}`;
  } else if (loaiKhachHangEL == "DN") {
    document.querySelector(
      "#kqBai4"
    ).innerHTML = `Mã khách hàng: ${maKhachHangEL}; Tiền cáp: $${tienDoanhNghiep(
      loaiKhachHangEL,
      soKenhCaoCapEL,
      soKetNoiEL
    ).toLocaleString()}`;
  } else {
    alert("hãy chọn loại khách hàng");
  }
});
